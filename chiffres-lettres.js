"use strict"

const messages = {
  accueil: "<div class='consigne'>Pour commencer, tu dois sélectionner un type d'exercice et un niveau.</div>",
  nouveauNombre: "<div class='consigne'>Clique sur le bouton [nouveau nombre] pour lancer l'exercice.</div>",
  verifier: "<div class='consigne'>Clique sur le bouton [Vérifier] lorsque tu penses avoir saisi la bonne réponse.</div>",
  erreur: ["Ce que tu as écrit ne correspond pas au nombre demandé.","Malheureusement, ce n'est toujours pas la bonne réponse.","C'est toujours inexact. </br><div class='consigne'>Tu peux encore essayer, ou abandonner et voir la bonne réponse en cliquant sur le bouton [Solution].</div>"],
  erreurLettres: "</br>Vérifie également que tu n'as pas fait d'erreur dans l'orthographe des mots.",
  bravo: "<span class='messages--vert'>Bravo !</span><div class='consigne'>Clique sur [Nouveau nombre], ou sélectionne un autre type d'exercice ou un autre niveau.</div>",
  bravoMaisChiffres: "<span class='messages--orange'>Ta réponse est acceptée, mais tu n'as pas correctement séparé les classes par des espaces.</span></br><div class='consigne'>Clique sur [Nouveau nombre] ou sélectionne un autre type d'exercice ou un autre niveau.</div>",
  bravoMaisLettres: "<span class='messages--orange'>Ta réponse est acceptée, mais il y a des erreurs dans les tirets.</span></br><div class='consigne'>Clique sur [Nouveau nombre] ou sélectionne un autre type d'exercice ou un autre niveau.</div>",
};

const paramExercice = {
  nombre: 0,
  nombreEnLettres: "",
  niveau: -1,
  typeExo: "",
  indiceExo: -1,
  ctNombres: 0,
}

// Variables nécessaires au calcul des résultats
const paramResultats = {
  ctTentatives: 0,
  ctTotalTentatives: [
                       [0,0,0,0,0,0],
                       [0,0,0,0,0,0]
                     ],
  ctEpreuves: [
                 [0,0,0,0,0,0],
                 [0,0,0,0,0,0]
               ],
  ctReussis: [
               [0,0,0,0,0,0],
               [0,0,0,0,0,0]
             ],
}

// Affichage de la zone d'exercice
const nombreReference = document.getElementById("modele");
const annonce = document.getElementById("annonces");

// Affichage des résultats
const nbTentatives = document.getElementById("nbtentatives");
const nbEpreuves = document.getElementById("nbepreuves");
const totalTentatives = document.getElementById("total-tentatives");
const tauxEfficacite = document.getElementById("efficacite");
const nbEchecs = document.getElementById("nbechecs");
const tauxReussite = document.getElementById("reussite");
const nomEleve =  document.getElementById("patronyme");
const tabResultats = document.getElementById('tab-resultats');

// Boutons
const btNouveau = document.getElementById("btnouveau");
const btVerifier = document.getElementById("btverifier");
const btSolution = document.getElementById("btsolution");

// Zone de saisie
const formSaisie = document.getElementById("form-saisie")

function alea(min,max) {
  let nb = min + (max-min+1) * Math.random();
  return Math.floor(nb);
}

function supprimeEspaces(chaine) {
  let s;
  s = chaine.replace(/[ ]/g,'');
  return s;
}

function formateChaine(chaine) {
  let s;
  s = chaine.replace(/[ ]+/g,'-');
  return s;
}

function formateNombre(nombre,taille) {
  let i = 0;
  let nbEspaces = 0;
  let chaine = "";
  let s = nombre.toString();
  nbEspaces = taille - chaine.length;
  for (i = 0; i < nbEspaces; i++) {
    chaine = chaine + " ";
  }
  chaine = chaine + s;
  return chaine;
}

function tireNombre() {
  let nombre;
  let seuil = 100;
  switch(paramExercice.niveau) {
    case 1:
      seuil = 100;
      break;
    case 2:
      seuil = 1000;
      break;
    case 3:
      seuil = 10000;
      break;
    case 4:
      seuil = 1000000;
      break;
    case 5:
      seuil = 1000000000;
      break;
    case 6:
      seuil = 1000000000000;
      break;
  }
  nombre = alea(1,seuil);
  return nombre;
}

function decomposeNombre(nombre) {
  const composition = {
    cMilliards: 0,
    cMillions: 0,
    cMille: 0,
    cUnites: 0,
  }
  let reste = 0;
  composition.cMilliards = Math.trunc(nombre / 1000000000);
  reste = nombre % 1000000000;
  composition.cMillions = Math.trunc(reste / 1000000);
  reste = nombre % 1000000;
  composition.cMille = Math.trunc(reste / 1000);
  composition.cUnites = nombre % 1000;;
  return composition;
}

function litClasse(nombre) {
  const chiffres = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99];
  const mots = ["","un","deux","trois","quatre","cinq","six","sept","huit","neuf","dix","onze","douze","treize","quatorze","quinze","seize","dix-sept","dix-huit","dix-neuf","vingt","vingt-et-un","vingt-deux","vingt-trois","vingt-quatre","vingt-cinq","vingt-six","vingt-sept","vingt-huit","vingt-neuf","trente","trente-et-un","trente-deux","trente-trois","trente-quatre","trente-cinq","trente-six","trente-sept","trente-huit","trente-neuf","quarante","quarante-et-un","quarante-deux","quarante-trois","quarante-quatre","quarante-cinq","quarante-six","quarante-sept","quarante-huit","quarante-neuf","cinquante","cinquante-et-un","cinquante-deux","cinquante-trois","cinquante-quatre","cinquante-cinq","cinquante-six","cinquante-sept","cinquante-huit","cinquante-neuf","soixante","soixante-et-un","soixante-deux","soixante-trois","soixante-quatre","soixante-cinq","soixante-six","soixante-sept","soixante-huit","soixante-neuf","soixante-dix","soixante-et-onze","soixante-douze","soixante-treize","soixante-quatorze","soixante-quinze","soixante-seize","soixante-dix-sept","soixante-dix-huit","soixante-dix-neuf","quatre-vingt","quatre-vingt-un","quatre-vingt-deux","quatre-vingt-trois","quatre-vingt-quatre","quatre-vingt-cinq","quatre-vingt-six","quatre-vingt-sept","quatre-vingt-huit","quatre-vingt-neuf","quatre-vingt-dix","quatre-vingt-onze","quatre-vingt-douze","quatre-vingt-treize","quatre-vingt-quatorze","quatre-vingt-quinze","quatre-vingt-seize","quatre-vingt-dix-sept","quatre-vingt-dix-huit","quatre-vingt-dix-neuf"];
  const centaines = Math.trunc(nombre / 100);
  let nombreEnLettres = "";
  let position=0;
  let reste = 0;
  if (centaines > 0) {
    if (centaines > 1) {
      position = chiffres.indexOf(centaines);
      nombreEnLettres = mots[position] + "-cent";
      }
    else {
      nombreEnLettres = "cent";
    }
  }
  reste = nombre % 100;
  if (reste === 0) {
    return nombreEnLettres;
  }
  position = chiffres.indexOf(reste);
  if (centaines > 0) {
    nombreEnLettres = nombreEnLettres + "-" + mots[position];
    }
    else {
      nombreEnLettres = mots[position];
  }
  return nombreEnLettres;
}

function litNombre(nombre) {
  let nombreEnLettres = "";
  let groupe = "";
  let finNombre = -1;
  const composition = decomposeNombre(nombre);
  if (composition.cMilliards > 0) {
    groupe = litClasse(composition.cMilliards);
    if (composition.cMilliards > 1) {
      nombreEnLettres = groupe + "-milliards";
      }
    else {
      nombreEnLettres = groupe + "-milliard";
    }
  }
  if (composition.cMillions > 0) {
    groupe = litClasse(composition.cMillions);
    if (composition.cMillions > 1) {
      nombreEnLettres = nombreEnLettres + "-" + groupe + "-millions";
      }
    else {
      nombreEnLettres = nombreEnLettres + "-" + groupe + "-million";
    }
  }
  if (composition.cMille > 0) {
    groupe  = litClasse(composition.cMille);
    if (composition.cMille > 1) {
      nombreEnLettres = nombreEnLettres + "-" + groupe + "-mille";
      }
    else {
      nombreEnLettres = nombreEnLettres + "-mille";
    }
  }
  if (composition.cUnites > 0) {
      nombreEnLettres = nombreEnLettres + "-" + litClasse(composition.cUnites);
  }
  finNombre = composition.cUnites % 100;
  if ((finNombre === 80) || ((finNombre === 0) && (Math.trunc(composition.cUnites / 100) > 1))) {
    nombreEnLettres = nombreEnLettres + "s";
  }
  nombreEnLettres = nombreEnLettres.replace(/^[-]*/,'');
  return nombreEnLettres;
}

function getOldOrthographe(nombre) {
  let chaine = nombre;
  chaine = chaine.replace(/-milliard-/,' milliard ');
  chaine = chaine.replace(/-milliards-/,' milliards ');
  chaine = chaine.replace(/-million-/,' million ');
  chaine = chaine.replace(/-millions-/,' millions ');
  chaine = chaine.replace(/-mille-/,' mille ');
  chaine = chaine.replace(/-cent-/g,' cent ');
  chaine = chaine.replace(/cent-/g,' cent ');
  chaine = chaine.replace(/-et-/g,' et ');
  return chaine;
}

function changeZoneSaisie() {
  let zoneSaisie;
  if (document.getElementById("reponse")) {
    formSaisie.removeChild(document.getElementById("reponse"));
  }
  if (paramExercice.typeExo === "chiffres") {
    zoneSaisie = document.createElement("input");
    zoneSaisie.name = "zoneExercice_formSaisie_input";
  }
  else {
    zoneSaisie = document.createElement("textarea");
    zoneSaisie.name = "zoneExercice_formSaisie_textarea";
    zoneSaisie.setAttribute("rows","2");
  }
  zoneSaisie.id = "reponse";
  zoneSaisie.setAttribute("type","text");
  zoneSaisie.focus();
  zoneSaisie.disabled = "true";
  zoneSaisie.addEventListener('copy', function(){return false;});
  zoneSaisie.addEventListener('paste', function(){return false;});
  zoneSaisie.addEventListener('cut', function(){return false;});
  formSaisie.appendChild(zoneSaisie);
  changeTailleSaisie();
}

function changeTailleSaisie() {
  let nomClasse = "";
  if (paramExercice.niveau === -1) {
    return;
  }
  if (( ! document.getElementById("reponse")) && (paramExercice.typeExo !== "")) {
    changeZoneSaisie();
  }
  const reponse = document.getElementById("reponse");
  nomClasse = reponse.name;
  switch (true) {
    case (paramExercice.niveau <= 2):
      nomClasse = nomClasse + "--etroit";
      break;
    case (paramExercice.niveau <= 4):
      nomClasse = nomClasse + "--moyen";
      break;
    case (paramExercice.niveau <= 6):
      nomClasse = nomClasse + "--large";
      break;
  }
  reponse.className = reponse.name + ' ' + nomClasse;
}

function construitTabResultats() {
  let i, j;
  let contenu = "";
  let idCell = "";
  let taux1 = 0;
  let taux2 = 0;
  let nbEchecs = 0;
  for (i = 0; i < 2; i++) {
    for (j = 0; j < 6; j++) {
      taux1 = 0;
      taux2 = 0;
      if ( paramResultats.ctTotalTentatives[i][j] > 0 ) {
        taux1 = Math.round((paramResultats.ctEpreuves[i][j] * 100) / paramResultats.ctTotalTentatives[i][j]);
      }
      contenu = paramResultats.ctEpreuves[i][j] + "<br />" + paramResultats.ctTotalTentatives[i][j] + "<br />";
      if ( taux1 ) {
        contenu = contenu +  taux1 + "%<br>";
      }
      else {
        contenu = contenu +  "<br />";
      }
      nbEchecs = paramResultats.ctEpreuves[i][j] - paramResultats.ctReussis[i][j];
      contenu = contenu + nbEchecs + "<br />";
      if ( paramResultats.ctEpreuves[i][j] > 0 ) {
        taux2 = Math.round((paramResultats.ctReussis[i][j] * 100) / paramResultats.ctEpreuves[i][j]);
      }
      if ( taux2 ) {
        contenu = contenu + taux2 + "%";
      }
      idCell = "resu-" + i + "-" + j;
      document.getElementById(idCell).innerHTML = contenu;
    }
  }
}

function effaceAncienNombre() {
  if (document.getElementById("reponse")) {
    document.getElementById("reponse").value = "";
  }
  // reponse.value = "";
  nbTentatives.innerHTML = "";
  nombreReference.innerHTML = "";
}

function afficheNombre() {
  const reponse = document.getElementById("reponse");
  reponse.value = "";
  reponse.disabled = false;
  reponse.focus();
  if (paramExercice.typeExo === "chiffres") {
    nombreReference.textContent = paramExercice.nombreEnLettres;
    }
  else {
    nombreReference.textContent = Intl.NumberFormat('fr-FR').format(paramExercice.nombre);
  }
  annonce.innerHTML = messages.verifier;
}

function afficheSolution() {
  if (paramExercice.typeExo === "chiffres") {
    nombreReference.innerHTML = paramExercice.nombreEnLettres + "<p>" +  Intl.NumberFormat('fr-FR').format(paramExercice.nombre) + "</p>";

  }
  else {
    nombreReference.innerHTML =  Intl.NumberFormat('fr-FR').format(paramExercice.nombre) + "<p>" + paramExercice.nombreEnLettres + "</p>";
  }
}

function nouveauNombre() {
  paramResultats.ctTentatives = 0;
  nbTentatives.innerHTML = "";
  paramExercice.nombre = tireNombre();
  paramExercice.nombreEnLettres = litNombre(paramExercice.nombre);
  btNouveau.disabled = true;
  btVerifier.disabled = false;
  afficheNombre();
}

function defCouleur(taux) {
  let couleur = "";
  switch (true) {
    case (taux < 40):
      couleur = "red";
      break;
    case (taux < 70):
      couleur = "orange";
      break;
    case (taux <= 100):
      couleur = "green";
  }
  return couleur;
}

function afficheResultats() {
  let taux1 = 0;
  let taux2 = 0;
  const exo = paramExercice.indiceExo;
  const niveau = paramExercice.niveau - 1;
  if ((exo === -1) || (niveau === -1)) {
    return;
  }
  if ( paramResultats.ctTotalTentatives[exo][niveau] > 0 ) {
    taux1 = Math.round((paramResultats.ctEpreuves[exo][niveau] * 100) / paramResultats.ctTotalTentatives[exo][niveau]);
  }
  nbEpreuves.innerHTML = paramResultats.ctEpreuves[exo][niveau];
  if ( taux1 ) { tauxEfficacite.innerHTML = taux1 + "%"; }
  tauxEfficacite.style.color = defCouleur(taux1);
  totalTentatives.innerHTML = paramResultats.ctTotalTentatives[exo][niveau];
  nbEchecs.innerHTML = paramResultats.ctEpreuves[exo][niveau] - paramResultats.ctReussis[exo][niveau];
  if ( paramResultats.ctEpreuves[exo][niveau] > 0 ) {
    taux2 = Math.round((paramResultats.ctReussis[exo][niveau] * 100) / paramResultats.ctEpreuves[exo][niveau]);
  }
  if ( taux2 ) { tauxReussite.innerHTML = taux2 + "%"; }
  tauxReussite.style.color = defCouleur(taux2);
}

function effaceResultats() {
  nbTentatives.innerHTML = "";
  nbEpreuves.innerHTML = "";
  nbTentatives.innerHTML = "";
  tauxEfficacite.innerHTML = "";
  nbEchecs.innerHTML = "";
  tauxReussite.innerHTML = "";
}

function lanceNiveau(niveau) {
  paramExercice.niveau = niveau;
  // changeTailleSaisie();
  if (paramExercice.typeExo !== "") {
    btNouveau.disabled = false;
    annonce.innerHTML = messages.nouveauNombre;
    effaceAncienNombre();
    effaceResultats();
    afficheResultats();
    changeTailleSaisie();
  }
}

function lanceExercice(choix) {
  // changeZoneSaisie(choix);
  // const reponse = document.getElementById("reponse");
  paramExercice.typeExo = choix;
  if (choix === "chiffres") {
    // reponse.classList.replace("large","etroit");
    paramExercice.indiceExo = 0;
  }
  else {
    // reponse.classList.replace("etroit","large");
    paramExercice.indiceExo = 1;
  }
  if (paramExercice.niveau !== -1) {
    btNouveau.disabled = false;
    annonce.innerHTML = messages.nouveauNombre;
    effaceAncienNombre();
    effaceResultats();
    afficheResultats();
    changeZoneSaisie();
    // changeTailleSaisie();
  }
}

function activeNouvelExo() {
  document.getElementById("reponse").disabled = true;
  btNouveau.disabled = false;
  btVerifier.disabled = true;
  btSolution.disabled = true;
  btNouveau.focus();
}

function inscritResultats(type) {
  paramResultats.ctTotalTentatives[paramExercice.indiceExo][paramExercice.niveau - 1] = paramResultats.ctTotalTentatives[paramExercice.indiceExo][paramExercice.niveau - 1] + paramResultats.ctTentatives + 1;
  paramResultats.ctEpreuves[paramExercice.indiceExo][paramExercice.niveau - 1] += 1;
  if ( type === "reussite") {
    paramResultats.ctReussis[paramExercice.indiceExo][paramExercice.niveau - 1] += 1;
  }
  afficheResultats();
}

function verifier() {
  const reponse = document.getElementById("reponse");
  let i = 0;
  let ctErreurs = paramResultats.ctTentatives;
  let proposition = reponse.value;
  let solution = "";
  let solutionBis = "";
  let solutionAlternative = "";
  let message = [];
  nbTentatives.innerHTML = paramResultats.ctTentatives + 1;
  if (paramExercice.typeExo === "chiffres") {
    solution = Intl.NumberFormat('fr-FR').format(paramExercice.nombre);
    proposition = reponse.value.replace(/[ ]/g,' ');
    // Charge les messages d'erreur
    for (i=0; i<=2; i++) {
      message[i] = messages.erreur[i];
    }
  }
  else {
    proposition = reponse.value.replace(/\n+$/,'');
    solution = paramExercice.nombreEnLettres;
    solutionBis = getOldOrthographe(paramExercice.nombreEnLettres);
    // Charge les messages d'erreur et ajoute les spécificités des nombres en lettres
    for (i=0; i<=2; i++) {
      message[i] = messages.erreur[i] + messages.erreurLettres;
    }
  }
  if ((proposition === solution) || ((proposition !== "") && (proposition === solutionBis))) {
    annonce.innerHTML = messages.bravo;
    inscritResultats("reussite");
    activeNouvelExo();
    return;
  }
  if (paramExercice.typeExo === "lettres") {
    proposition = formateChaine(proposition);
    solutionAlternative = formateChaine(solution);
    if (proposition === solutionAlternative) {
      annonce.innerHTML = messages.bravoMaisLettres;
      afficheSolution();
      inscritResultats("reussite");
      activeNouvelExo();
      return;
    }
  }
  else {
    solution = paramExercice.nombre;
    proposition = parseInt(supprimeEspaces(reponse.value));
    if (proposition === solution) {
      annonce.innerHTML = messages.bravoMaisChiffres;
      afficheSolution();
      inscritResultats("reussite");
      activeNouvelExo();
      return;
    }
  }
  if (ctErreurs >= 2) {
    ctErreurs = 2;
    btSolution.disabled = false;
  };
  annonce.innerHTML = message[ctErreurs];
  paramResultats.ctTentatives += 1;
}

function abandonner() {
  afficheSolution();
  inscritResultats("echec");
  activeNouvelExo();
}

function soumetReponse(ev) {
  ev.preventDefault();
  verifier();
}

function quitteModaleNom() {
  document.getElementById("ecran-blanc").classList.replace("visible","invisible");
  document.getElementById("saisie-nom").classList.replace("visible","invisible");
}

function quittePageResultats() {
  document.getElementById("ecran-blanc").classList.replace("visible","invisible");
  document.getElementById("modale-resultats").classList.replace("visible","invisible");
}

function quitteModaleInfos() {
  document.getElementById("ecran-blanc").classList.replace("visible","invisible");
  document.getElementById("modale-infos").classList.replace("visible","invisible");
}

function demandeNom() {
  document.getElementById("ecran-blanc").classList.replace("invisible","visible");
  document.getElementById("saisie-nom").classList.replace("invisible","visible");
  document.getElementById("patronyme").focus();
}

function pageResultats(ev) {
  let laDate = new Date();
  let zoneDate = document.getElementById('date');
  let Nom = document.getElementById('nom');
  ev.preventDefault();
  quitteModaleNom();
  document.getElementById("ecran-blanc").classList.replace("invisible","visible");
  document.getElementById("modale-resultats").classList.replace("invisible","visible");
  zoneDate.innerHTML = "Date : " + laDate.getDate() +"/" + (laDate.getMonth()+1) +"/" + laDate.getFullYear();
  Nom.innerHTML = nomEleve.value;
  construitTabResultats();
}

function afficheInfos() {
  document.getElementById("ecran-blanc").classList.replace("invisible","visible");
  document.getElementById("modale-infos").classList.replace("invisible","visible");
  document.getElementById("btquitter").focus();
}

function initialise() {
  annonce.innerHTML = messages.accueil;
  effaceResultats();
}

window.onload = initialise();